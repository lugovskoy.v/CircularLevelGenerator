﻿using UnityEngine;


public class MapGenerator : MonoBehaviour {

    int[,] map;

    [Range(0, 100)]
    public int randomFillPercent = 45;

    public string seed;
    public bool useRandomSeed;

    public int width;
    public int height;

    public int smoothCount = 5;
    public int borderSize = 5;

    MeshGenerator meshGenerator = null;

    private void Start() {
        meshGenerator = GetComponent<MeshGenerator>();
        GenerateMap();

    }

    void GenerateMap() {
        map = new int[height, width];
        RandomFillMap();
        for(int i = 0; i < smoothCount; ++i) {
            SmoothMap();
        }

        
        int[,] borderedMap = new int[height + borderSize * 2, width + borderSize * 2];

        for(int i = 0; i < borderedMap.GetLength(0); ++i) {
            for(int j = 0; j < borderedMap.GetLength(1); ++j) {
                if(j >= borderSize && j < width + borderSize && i >= borderSize && i < height + borderSize) {
                    borderedMap[i, j] = map[i - borderSize, j - borderSize];
                } else
                {
                    borderedMap[i, j] = 1;
                }
            }
        }

        meshGenerator.GenerateMesh(borderedMap, 1f);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)) {
            GenerateMap();
        }
        
    }

    void RandomFillMap()
    {
        if(useRandomSeed) {
            seed = Time.time.ToString();
        }
        System.Random random = new System.Random(seed.GetHashCode());
        for(int i = 0; i < height; ++i) {
            for(int j = 0; j < width; ++j) {
                if(i == 0 || j == 0 || i == height - 1 || j == width - 1) {
                    map[i, j] = 1;
                } else {
                    map[i, j] = (random.Next(0, 100) < randomFillPercent) ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap() {
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                int neighbourWalls = GetSurroundingWallCount(j, i);
                if (neighbourWalls > 4)
                    map[i, j] = 1;
                else if (neighbourWalls < 4)
                    map[i, j] = 0;
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int i = gridY - 1; i <= gridY + 1; i++) {
            for (int j = gridX - 1; j <= gridX + 1; j++) {
                if (j >= 0 && j < width && i >= 0 && i < height) {
                    if ((j != gridX || i != gridY) && map[i, j] == 1) {
                        ++wallCount ;
                    }
                } else {
                    wallCount++;
                }
            }
        }
        return wallCount;
    }

    private void OnDrawGizmos()
    {
        //if (map != null) {
        //    for (int i = 0; i < height; ++i) {
        //        for (int j = 0; j < width; ++j) {
        //            Gizmos.color = map[i, j] == 1 ? Color.black : Color.white;
        //            Vector3 pos = new Vector3(-width/2 + j + 0.5f, 0, -height / 2 + i + 0.5f);
        //            Gizmos.DrawCube(pos, Vector3.one);
        //        }
        //    }
        //}
    }

}
