﻿using UnityEngine;

public class CircularMapGenerator : MonoBehaviour {

    int[,] map;

    [Range(0, 100)]
    public int randomFillPercent = 45;

    public string seed;
    public bool useRandomSeed;

    public int radius;
    public int innerRadius;

    public int smoothCount = 5;
    public int borderSize = 5;

    MeshGenerator meshGenerator = null;

    private void Start()
    {
        meshGenerator = GetComponent<MeshGenerator>();
        GenerateMap();

    }

    void GenerateMap()
    {
        map = new int[radius*2+1, radius*2+1];
        MakeCircleWalls(radius, radius, radius);
        MakeCircleWalls(radius, radius, innerRadius);
        RandomFillMap();
        for (int i = 0; i < smoothCount; ++i)
        {
            SmoothMap();
        }
        MakeCircleWalls(radius, radius, radius);

        meshGenerator.GenerateMesh(map, 1f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GenerateMap();
        }

    }

    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = Time.time.ToString();
        }
        System.Random random = new System.Random(seed.GetHashCode());
        Random.InitState(seed.GetHashCode());

        float S, outerS;
        S = Mathf.PI * radius * radius;
        outerS = S - Mathf.PI * innerRadius * innerRadius;
        int pointsLeft = (int)(outerS * randomFillPercent / 100f);
        float sqrInnerS = innerRadius * innerRadius;
        while(pointsLeft > 0) {
            
            Vector2 v = Random.insideUnitCircle * radius;
            int x = Mathf.RoundToInt(v.x) + radius;
            int y = Mathf.RoundToInt(v.y) + radius;
            if(v.sqrMagnitude < sqrInnerS || map[y, x] == 1) {
                continue;
            }
            map[y, x] = 1;
            --pointsLeft;
        }

        //for (int i = 0; i < radius; ++i)
        //{
        //    for (int j = 0; j < radius; ++j)
        //    {
        //        if (i == 0 || j == 0 || i == height - 1 || j == width - 1)
        //        {
        //            map[i, j] = 1;
        //        }
        //        else
        //        {
        //            map[i, j] = (random.Next(0, 100) < randomFillPercent) ? 1 : 0;
        //        }
        //    }
        //}
    }

    void SmoothMap()
    {
        
        float sqrInnerS = innerRadius * innerRadius + 1;
        float sqrS = radius * radius;
        for (int i = 0; i < radius*2+1; ++i)
        {
            for (int j = 0; j < radius*2+1; ++j)
            {
                var sqrDist = new Vector2Int(radius - j, radius - i).sqrMagnitude;
                if(sqrDist >= sqrS || sqrDist <= sqrInnerS ) {
                    continue;
                }

                int neighbourWalls = GetSurroundingWallCount(j, i);
                if (neighbourWalls > 4)
                    map[i, j] = 1;
                else if (neighbourWalls < 4)
                    map[i, j] = 0;
            }
        }
    }

    int GetSurroundingWallCount(int gridX, int gridY)
    {
        int wallCount = 0;
        for (int i = gridY - 1; i <= gridY + 1; i++)
        {
            for (int j = gridX - 1; j <= gridX + 1; j++)
            {
                if (j >= 0 && j < radius*2+1 && i >= 0 && i < radius*2+1)
                {
                    if ((j != gridX || i != gridY) && map[i, j] == 1)
                    {
                        ++wallCount;
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }
        return wallCount;
    }


    void MakeCircleWalls(int x0, int y0, int radius)
    {
        int x = radius - 1;
        int y = 0;
        int dx = 1;
        int dy = 1;
        int err = dx - (radius << 1);

        while (x >= y)
        {
            map[x0 + x, y0 + y] = 1;
            map[x0 + y, y0 + x] = 1;
            map[x0 - y, y0 + x] = 1;
            map[x0 - x, y0 + y] = 1;
            map[x0 - x, y0 - y] = 1;
            map[x0 - y, y0 - x] = 1;
            map[x0 + y, y0 - x] = 1;
            map[x0 + x, y0 - y] = 1;

            if (err <= 0)
            {
                y++;
                err += dy;
                dy += 2;
            }

            if (err > 0)
            {
                x--;
                dx += 2;
                err += dx - (radius << 1);
            }
        }
    }
}
